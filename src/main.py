from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from kivy.core.window import Window
from kivy.uix.image import Image
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.scrollview import ScrollView
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.popup import Popup
from kivy.uix.floatlayout import FloatLayout
from exif import Image as Image_exif
from kivy.app import App
from os.path import join, dirname
from glob import glob
import time
import kivy
import os
kivy.require('1.11.1')

class NoInfoPopUp(Popup):
    pass

class ImageInfoPage(FloatLayout):
    def __init__(self, **kwargs):
        super(ImageInfoPage, self).__init__(**kwargs)

    def display_image_info(self, img, source):
        self.source = source
        self.x_res_label = self.ids['x_res_label']
        self.y_res_label = self.ids['y_res_label']
        self.aperture_label = self.ids['aperture_label']
        self.make_label = self.ids['make_label']
        self.model_label = self.ids['model_label']

        self.x_res_label.text = str(img.x_resolution)
        self.y_res_label.text = str(img.y_resolution)
        self.aperture_label.text = str(img.aperture_value)
        self.make_label.text = str(img.make)
        self.model_label.text = str(img.model)

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            MainObj.image_view_page.set_image_source(self.source)
            MainObj.screen_manager.current = 'ImageView'

class DeletePopUp(Popup):
    source = StringProperty(None)

    def __init__(self, **kwargs):
        super(DeletePopUp, self).__init__(**kwargs)

    def cancel_delete(self, *args):
        self.dismiss()

    def confirm_delete(self, *args):
        if os.path.exists(self.source):
            os.remove(self.source)
        file_names = MainObj.get_image_files()
        MainObj.grid_view_page.update_grid(file_names)
        MainObj.screen_manager.current = 'GridView'
        self.dismiss()

class ImageViewPage(FloatLayout):
    scatter = ObjectProperty(None)
    image = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(ImageViewPage, self).__init__(**kwargs)

    def set_image_source(self, source):
        self.image.source = source

    def delete_button_behaviour(self, *args):
        deletepopup = DeletePopUp(source=self.image.source)
        deletepopup.open()

    def rotate_button_behaviour(self, *args):
        self.scatter.rotation -= 90

    def back_button_behaviour(self, *args):
        MainObj.screen_manager.current = 'GridView'
        self.scatter.rotation = 0

    def info_button_behaviour(self, *args):
        with open(self.image.source, 'rb') as image_file:
            self.img = Image_exif(image_file)

        if self.img.has_exif:
            MainObj.image_info_page.display_image_info(self.img,
                                                       self.image.source)
            MainObj.screen_manager.current = 'InfoPage'
        else:
            popup = NoInfoPopUp()
            popup.open()

class CameraPopUp(Popup):
    def __init__(self, **kwargs):
        super(CameraPopUp, self).__init__(**kwargs)

    def back(self):
        self.dismiss()

    def go_to_home(self):
        file_names = MainObj.get_image_files()
        MainObj.grid_view_page.update_grid(file_names)
        MainObj.screen_manager.current = 'GridView'
        self.dismiss()

class CameraPage(BoxLayout):
    def __init__(self, **kwargs):
        super(CameraPage, self).__init__(**kwargs)

    def capture(self):
        appdir = MainObj.directory
        camera = self.ids['camera']
        timestr = time.strftime("%Y%m%d_%H%M%S")
        camera.export_to_png(join(appdir,"../images/IMG_{}.png".format(timestr)))
        popup = CameraPopUp()
        popup.open()

    def load_grid_view(self):
        file_names = MainObj.get_image_files()
        MainObj.grid_view_page.update_grid(file_names)
        MainObj.screen_manager.current = 'GridView'

class AddImage(Image):
    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            MainObj.image_view_page.set_image_source(self.source)
            MainObj.screen_manager.current = 'ImageView'

class SortByDropDown(DropDown):
    file_name_dict = {}
    curdir = dirname(__file__)

    def __init__(self, **kwargs):
        super(SortByDropDown, self).__init__(**kwargs)

    def sort_by_date(self):
        appdir = MainObj.directory
        for filename in glob(join(appdir, '../images', '*')):
            image_stat = os.stat(filename)
            self.file_name_dict.update({filename: (image_stat.st_mtime)})
        MainObj.grid_view_page.update_grid(sorted(self.file_name_dict,
                                                  key=self.file_name_dict.get))

    def sort_by_size(self):
        appdir = MainObj.directory
        for filename in glob(join(appdir, '../images', '*')):
            image_stat = os.stat(filename)
            self.file_name_dict.update({filename: (image_stat.st_size)})
        MainObj.grid_view_page.update_grid(sorted(self.file_name_dict,
                                                  key=self.file_name_dict.get))

class GridViewPage(FloatLayout):
    grid = ObjectProperty(None)
    sortbybutton = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(GridViewPage, self).__init__(**kwargs)
        Window.clearcolor = (1, 1, 1, 1)

    def update_grid(self, file_name):
        self.grid.clear_widgets()
        sortbydropdown = SortByDropDown()

        self.sortbybutton.bind(on_release=sortbydropdown.open)

        for image_source in file_name:
            thumb = AddImage(source=image_source)
            self.grid.add_widget(thumb)

    def open_camera(self):
        MainObj.screen_manager.current = 'CameraPage'

class ImageViewerApp(App):
    def build(self):
        self.screen_manager = ScreenManager(transition=NoTransition())

        self.grid_view_page = GridViewPage()
        file_names = self.get_image_files()
        self.grid_view_page.update_grid(file_names)

        screen = Screen(name='GridView')
        screen.add_widget(self.grid_view_page)
        self.screen_manager.add_widget(screen)

        self.image_view_page = ImageViewPage()
        screen = Screen(name='ImageView')
        screen.add_widget(self.image_view_page)
        self.screen_manager.add_widget(screen)

        self.image_info_page = ImageInfoPage()
        screen = Screen(name='InfoPage')
        screen.add_widget(self.image_info_page)
        self.screen_manager.add_widget(screen)

        self.camera_page = CameraPage()
        screen = Screen(name='CameraPage')
        screen.add_widget(self.camera_page)
        self.screen_manager.add_widget(screen)

        return self.screen_manager

    def get_image_files(self):
        appdir = MainObj.directory
        file_names = []
        for image in glob(join(appdir, '../images', '*')):
            file_names.append(image)
        return file_names

if __name__ == '__main__':
    MainObj = ImageViewerApp()
    MainObj.run()
