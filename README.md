# ImageViewerKivy

The project aims to create a basic image viewer application with cross platform support using Kivy a Python based framework.

As of this release the project looks in a pictures folder in the images directory as your project to search for JPEG and PNG images.
You can change this path in the code to any folder where images are present.
Error handling will be done soon.

Make sure you have the latest version of python3 running on your system.
To install python3 go to https://www.python.org/downloads/ and choose the latest stable release.

Clone using:

    git clone git@gitlab.com:iamkushalchand/imageviewerkivy.git

Install the requirements using:

    pip install -r requirements.txt

To run the application:

    python3 main.py
